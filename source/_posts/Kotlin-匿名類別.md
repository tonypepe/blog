---
title: Kotlin 匿名類別
date: 2019-07-31 01:19:35
tags:
---

## 一般匿名類別寫法

``` kotlin
button.setOnClickListener(object : View.OnClickListener {
    override fun onClick(view: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
})
```

<!-- more -->

{% note success %}
**用 object 關鍵字實作匿名類別**
{% endnote %}

## 在 Kotlin 中，若匿名類別只有一個方法，可簡化成這樣

``` kotlin
button.setOnClickListener({ view ->
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
})
```

## 若只有一個參數可省略

``` kotlin
button.setOnClickListener({
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
})
```

{% note success %}
**若要呼叫省略的參數，則用 " it "**
{% endnote %}

## 若匿名類別為方法的最後一個參數可移出括號外

``` kotlin
button.setOnClickListener() {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
}
```

## 若參數只有匿名類別，可省略括號

``` kotlin
button.setOnClickListener{
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
}
```
