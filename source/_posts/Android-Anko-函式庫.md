---
title: Android Anko 函式庫
date: 2019-08-01 18:58:41
tags:
  - android
  - kotlin
  - anko
  - jetpack
---

## 官方介紹

> Anko is a Kotlin library which makes Android application development faster and easier. It makes your code clean and easy to read, and lets you forget about rough edges of the Android SDK for Java.
> Anko Commons: a lightweight library full of helpers for intents, dialogs, logging and so on;
> Anko Layouts: a fast and type-safe way to write dynamic Android layouts;
> Anko SQLite: a query DSL and parser collection for Android SQLite;
> Anko Coroutines: utilities based on the kotlinx.coroutines library.

## 導入 Anko

{% code lang:gradle build.gradle(project) %}
buildscript {
    ext.anko_version = '0.10.8'
}
{% endcode %}

{% code lang:gradle build.gradle(app) %}
dependencies {
    implementation "org.jetbrains.anko:anko:$anko_version"
}
{% endcode %}

<!-- more -->

{% note warning %}
最新版本請參考官方 [GitHub](https://github.com/Kotlin/anko)
{% endnote %}

## Anko Intent

### 利用 intentFor<>() 的語法

``` kotlin
startActivity(intentFor<SomeOtherActivity>()
```

### 若要加入 Extra

``` kotlin
startActivity<SomeOtherActivity>(
    "id" to 5,
    "city" to "Denpasar"
)
```

### 其他常用 Intent

| Goal            | Solution                        |
| --------------- | ------------------------------- |
| Make a call     | makeCall(number)                |
| Send a text     | sendSMS(number, [text])         |
| Browse the web  | browse(url)                     |
| Share some text | share(text, [subject])          |
| Send an email   | email(email, [subject], [text]) |

## Anko 對話框

### Toast

``` kotlin
toast("Hi there!")
toast(R.string.message)
longToast("Wow, such duration")
```

### Alert

``` kotlin
alert("Hi, I'm Roy", "Have you tried turning it off and on again?") {
    yesButton { toast("Oh…") }
    noButton {}
}.show()
```

### AsyncTask

``` kotlin
doAsync {
    var result = runLongTask()
    uiThread {
        toast(result)
    }
}
```
